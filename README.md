# very-raw-gcode-post-process

Post-process gcode with c++.

I use it like this:
```
g++ -std=c++2a postprocess.cpp && ./a.out in.gcode > out.gcode
```
