#include <iostream>
#include <array>
#include <cmath>
#include <fstream>
#include <sstream>
#include <cstring>


static inline auto toDouble(std::string const &s) -> double {
  std::size_t charCount = 0;
  double res = std::stod(s, &charCount);
  if (s.size() != charCount) {
    throw std::invalid_argument("Could not parse all characters");
  }
  return res;
}

// remove all ' ', '\n', '\r', '\f', '\t', '\v'
// trim from start (in place)
static inline void lTrimWhitespace(std::string &s) {
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return std::isspace(ch) == 0;
          }));
}

static inline float x_transform(std::string const& strNum){
  return -toDouble(strNum) - 200.0;
}

static inline float y_transform(std::string const& strNum){
  return toDouble(strNum);
}

static inline float z_transform(std::string const& strNum){
  double const z_val = toDouble(strNum);
  return z_val;
}


int main(int argc, char** argv){
  if ((argc != 2 and argc != 3) or
      (argc >= 2 and strncmp(argv[1], "--help", 6) == 0)) {
    std::cout << "Usage:\n";
    std::cout << argv[0] << " [--minmax|--help] <input-gcode>\n";

    return 0;
  }
  bool minmax = false;
  if (argc == 3 and strncmp(argv[1], "--minmax", 8) == 0) {
    minmax = true;
  }
  double x_min = 0.0;
  double x_max = 0.0;
  double y_min = 0.0;
  double y_max = 0.0;

  auto fileNameArgIndex = 1;
  if (minmax) {
    fileNameArgIndex = 2;
  }
  std::ifstream fileStream{argv[fileNameArgIndex]};

  for (std::string line; std::getline(fileStream, line);) {
    if (line.substr(0, 3) == "G1 ") {
      line.erase(0, 3);
      std::string X{};
      std::string Y{};
      std::string Z{};
      std::string E{};
      std::string F{};
      size_t pos = 0;
      std::string token{};
      while ((pos = line.find(' ')) != std::string::npos) {
        token = line.substr(0, pos);
        if (token[0] == 'X' or token[0] == 'x') {
          X = line.substr(1, pos-1);
        } else if  (token[0] == 'Y' or token[0] == 'y') {
          Y = line.substr(1, pos-1);
        } else if  (token[0] == 'Z' or token[0] == 'z') {
          Z = line.substr(1, pos-1);
        } else if  (token[0] == 'E' or token[0] == 'e') {
          E = line.substr(1, pos-1);
        } else if  (token[0] == 'F' or token[0] == 'f') {
          F = line.substr(1, pos-1);
        }
        line.erase(0, pos + 1);
      }
      token = line.substr(0, pos);
      if (token[0] == 'X' or token[0] == 'x') {
        X = line.substr(1, pos-1);
      } else if  (token[0] == 'Y' or token[0] == 'y') {
        Y = line.substr(1, pos-1);
      } else if  (token[0] == 'Z' or token[0] == 'z') {
        Z = line.substr(1, pos-1);
      } else if  (token[0] == 'E' or token[0] == 'e') {
        E = line.substr(1, pos-1);
      } else if  (token[0] == 'F' or token[0] == 'f') {
        F = line.substr(1, pos-1);
      }

      if (not minmax) {
        std::cout << "G1";
      }
      if (not X.empty()){
        lTrimWhitespace(X);
        if (minmax) {
          double const x_val = toDouble(X);
          if (x_val < x_min) {
            x_min = x_val;
          }
          if (x_val > x_max) {
            x_max = x_val;
          }
        } else {
          std::cout << " X" << x_transform(X);
        }
      }
      if (not Y.empty()) {
        lTrimWhitespace(Y);
        if (minmax) {
          double const y_val = toDouble(Y);
          if (y_val < y_min) {
            y_min = y_val;
          }
          if (y_val > y_max) {
            y_max = y_val;
          }
        } else {
          std::cout << " Y" << y_transform(Y);
        }
      }
      if (not minmax) {
        if (not Z.empty()) {
          lTrimWhitespace(Z);
          std::cout << " Z" << z_transform(Z);
        }
        if (not E.empty()) {
          lTrimWhitespace(E);
          std::cout << " E" << toDouble(E);
        }
        if (not F.empty()) {
          lTrimWhitespace(F);
          std::cout << " F" << F;
        }
        std::cout << '\n';
      }
    }  else {
      if (not minmax) {
        std::cout << line << '\n';
      }
    }
  }

  if(minmax){
    std::cout << ";X min: " << x_min;
    std::cout << "\n;X max: " << x_max;
    std::cout << "\n;Y min: " << y_min;
    std::cout << "\n;Y max: " << y_max << '\n';
  }

  return 0;
}
